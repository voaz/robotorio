shader_type canvas_item;
render_mode unshaded;

uniform bool hover = false;

void fragment() {
	if (hover) {
		COLOR.rbga = texture(TEXTURE, UV);
	} else {
		COLOR.rgba = texture(TEXTURE, UV);
	}
}
