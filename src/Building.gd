extends "Item.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var craft_item = "iron_ore"
export var building_type = "drill"
export var craft_speed = 3
export var required_items = [{"coal":1}]

var time_remain = craft_speed

signal finished(item)
signal paused()
signal started()
# Called when the node enters the scene tree for the first time.
func _ready():
	._ready()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
