extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var id = ""
export var title = ""
export var icon = ""
export var recipe = {}
export var craft_time = 0
export var producing_amount = 1
export var stack_size = 1
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func load_data(data):
	id = data["id"]
	title = data["title"]
	var atlas = AtlasTexture.new()

	atlas.atlas = load(data["icon"])
	var rect = data["atlas_region"]
	atlas.region = Rect2(rect[0], rect[1], rect[2], rect[3])
	texture = atlas
	recipe = data["recipe"]
	craft_time = data["craft_time"]
	producing_amount = data["producing_amount"]
	stack_size = data["stack_size"]
	#texture = load(icon)
	print("loaded ", id)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_mouse_entered():
	self.material.set("shader_param/hover", 1)


func _on_Area2D_mouse_exited():
	self.material.set("shader_param/hover", 0)
