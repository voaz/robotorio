extends RigidBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var speed = 50
var max_zoom = 2
var min_zoom = 0.2
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	linear_velocity = speed* Vector2(Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
								Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up"))

func updateCamera(scale_value):
	var camera_zoom = get_node("Camera2D").zoom
	var temp_zoom = camera_zoom + Vector2(scale_value, scale_value)
	if temp_zoom.x >= min_zoom and temp_zoom.x < max_zoom:
		get_node("Camera2D").zoom = temp_zoom


func _on_Area2D_mouse_entered():
	self.material.set("shader_param/hover", 1)


func _on_Area2D_mouse_exited():
	self.material.set("shader_param/hover", 0)

