extends TileMap

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var tileSet
var belt_scene
var belt_array = {}
var golden_belts = []
var new_belts = []
var ore_fields = {}
var belt_timer = Timer
export var build_type = "belt"
export var map_seed = 642
var available_items = []
# Called when the node enters the scene tree for the first time.
func _ready():
	belt_scene = load("res://Scenes/Belt.tscn")
	tileSet = get_tileset()
	belt_timer = Timer.new()
	belt_timer.wait_time = 0.03
	belt_timer.autostart = true
	add_child(belt_timer)

	var noise = OpenSimplexNoise.new()
	noise.seed = map_seed
	noise.octaves = 4
	noise.period = 80.0
	noise.persistence = 0.8
	var strng = ""
	var i = 0.0
	var j = 0.0
	for z in range(200):
		for zz in range(400):
			if noise.get_noise_2d(z, zz) > 0:
				set_cell(zz, z, 4)
				strng += "W"
			else:
				strng += "_"
			i+=0.1
		j += 0.1
		strng += "\n"
	var file = File.new()
	file.open("res://Data/Map", File.WRITE)
	file.store_string(strng)
	file.close()

	update_bitmask_region()
	for k in range(8):
		var belt = belt_scene.instance()
		belt.frame = k
		belt.index = k
		new_belts.append(belt)
		golden_belts.append(belt)
	belt_timer.connect("timeout", self, "on_frame_changed")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#if Input.is_action_pressed("mouse_left"):
	#	if build_type == "belt":
	#		add_belt(get_local_mouse_position())
	#	elif build_type == "drill":
	#		construct("drill", get_local_mouse_position())
	if Input.is_action_pressed("mouse_right"):
		remove_item_by_pos(get_local_mouse_position())
	if Input.is_action_just_pressed("drop_key"):
		place_item(get_global_mouse_position())
	if Input.is_action_just_pressed("key_1"):
		key_activated(1)
	if Input.is_action_just_pressed("key_2"):
		key_activated(2)
	if Input.is_action_just_pressed("open_inventory"):
		var wind = get_parent().find_node("WindowDialog")
		if wind.visible == true:
			wind.visible = false
			return
		wind.popup()


func add_belt(pos):
	var new_pos = Vector2(int(pos.x / 32) * 32, int(pos.y / 32) * 32)
	if belt_array.has(new_pos):
		return
	var prev_belt = Vector2(new_pos.x - 32, new_pos.y)
	var belt

	if !belt_array.has(prev_belt):
		belt = golden_belts[0].duplicate()
	else:
		var indx = belt_array[prev_belt].index + 3
		if indx > 7:
			indx -= 8
		belt = golden_belts[indx].duplicate()
		belt_array[prev_belt].connect("item_on_boarder", belt, "on_item_on_boarder")

	belt.position = new_pos
	print(new_pos)
	belt_array[new_pos] = belt
	get_parent().add_child(belt)

	print_debug("mouse clicked and belt should be there!")

func sync_belts(belt):
	var prev_belt = Vector2(belt.position.x - 31, belt.position.y)
	if belt_array.has(prev_belt):
		var frame = belt_array[prev_belt].frame + 4
		if frame > 7:
			frame = frame - 8
		print_debug("frames ", belt_array[prev_belt].frame, " : ", frame)
		belt.frame = frame
	belt_timer.connect("timeout", self, "on_frame_changed")
	new_belts.append(belt)


func remove_item_by_pos(pos):
	var new_pos = Vector2(int(pos.x / 32) * 32, int(pos.y / 32) * 32)
	var item = belt_array.get(new_pos)
	if item == null:
		return
	get_parent().remove_child(item)
	if belt_array.erase(new_pos):
		print_debug("Success: Element deleted")
	else:
		print_debug("Warning: Element is not deleted ", belt_array.keys())

func on_frame_changed():
	for belt in new_belts:
		belt.playing = true;
	new_belts.empty()
	belt_timer.disconnect("timeout", self, "on_frame_changed")
	print("on_frame_changed called")

func place_item(pos):
	var tile_pos = Vector2(int(pos.x / 32) * 32, int(pos.y / 32) * 32)
	var obj = self
	var item = load("res://Scenes/Item.tscn").instance()
	if belt_array.has(tile_pos):
		obj = belt_array[tile_pos]
	item.position = pos - obj.position
	obj.add_child(item)

func key_activated(key):
	if key == 1:
		build_type = "belt"
	elif key == 2:
		build_type = "drill"

func construct(type, pos):
	var tile_pos = []
	tile_pos.push_back(Vector2(int(pos.x / 32) * 32, int(pos.y / 32) * 32))
	tile_pos.push_back(tile_pos[0] + Vector2(32,0))
	tile_pos.push_back(tile_pos[0] + Vector2(0,-32))
	tile_pos.push_back(tile_pos[0] + Vector2(32,-32))
	for tpos in tile_pos:
		if belt_array.has(tpos):
			print_debug("Error: Can't create here")
			return
	if type == "drill":
		var building = load("res://Scenes/Building.tscn").instance()
		building.position = tile_pos[0]
		add_child(building)
