extends Node


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
export var items = {} # Stores id : item

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
		load_items("res://Data/items.json")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass

func load_items(path):
	var json_raw = loadRawJSON(path)
	var parsed = JSON.parse(json_raw)

	if parsed.error == null:
		print(parsed.error_string)
		return
	if parsed.result is Array:
		for data in parsed.result:
			var item = load("res://Scenes/Item.tscn").instance()
			item.load_data(data)
			items[item.id] = item

func loadRawJSON(path):
	print("load_items(", path, ")")
	var file = File.new()
	print("file_exists: ", file.file_exists(path))
	if file.open(path, File.READ) != OK:
		print("Failed to open file")
	var json_raw = file.get_as_text()
	file.close()
	return json_raw
