extends Control


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var inv = find_node("Inventory")
	var rec_book = find_node("ReceiptBook")
	var craft = find_node("Crafter")
	craft.initialize_connects(rec_book)
	craft.initialize_connects(inv)
	rec_book.connect("recipe_clicked", craft, "set_target_item")
	inv.set_connections(rec_book)
	inv.set_connections(craft)
	craft.connect("deposit", inv, "on_deposit")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
