extends Control
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var cell = load("res://Scenes/Cell.tscn")

#cell count
export var amount = 5

#amount of available sources for crafting
var _source = {}
var _cell_array = []

#signals:
signal recipe_clicked(item_id)
# Called when the node enters the scene tree for the first time.
func _ready():
	load_receipts(GlobalData.items.values())


func populate_grid(size = 0):
	if amount <= 0:
		return
	var container = find_node("GridContainer") as GridContainer
	for i in range(amount):
		container.add_child(cell.instance())


func load_receipts(item_list):
	var container = find_node("GridContainer") as GridContainer
	for index in range(item_list.size()):
		var item = item_list[index] # why not duplicate() ?
		var temp_cell = cell.instance()
		item.scale = Vector2(2, 2)
		temp_cell.add_item(item)
		temp_cell.connect("clicked", self, "receipt_clicked")
		_cell_array.push_back(temp_cell)
		container.add_child(temp_cell)

func receipt_clicked(item):
	print("clicked item: ", item.id)
	emit_signal("recipe_clicked", item.id)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func update_source(items):
	_source = items
	update_book()

#update counts for each cell depends on available sources
func update_book():
	for tcell in _cell_array:
		var item = tcell.get_item()
		if item == null:
			continue
		var recipe = item.recipe
		var ids = recipe.keys()
		var min_count = 0
		for id in ids:
			if !_source.has(id):
				min_count = 0
				return
			else:
				min_count = _source[id] / recipe[id]
		tcell.set_count(min_count)

# SLots
func _on_Control_clicked(item):
	pass # Replace with function body.

func on_craft_finished(item):
	pass


