extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var cell_count = 10

var cell = load("res://Scenes/Cell.tscn")
var _cell_array = []
var _contains = {}
# Called when the node enters the scene tree for the first time.
func _ready():
	populate_grid(cell_count)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func populate_grid(size = 0):
	if cell_count <= 0:
		return
	var container = find_node("GridContainer") as GridContainer
	for i in range(cell_count):
		var tcell = cell.instance()
		_cell_array.append(tcell)
		container.add_child(tcell)


#update counts for each cell depends on available sources
func update_grid():
	var i = 0
	for item_id in _contains.keys():
#		var stack = GlobalData.items[item_id].stack_size
#		var cells = 0
#		if _contains[item_id] % stack > 0:
#			cells += 1
#		cells += _contains[item_id] / GlobalData.items[item_id].stack_size
		_cell_array[i].add_item(GlobalData.items[item_id].duplicate())
		_cell_array[i].set_count(_contains[item_id])
		i += 1

func empty():
	for cell in _cell_array:
		cell.add_item(null)
		cell.set_count(null)


func _on_Inventory_contains(items):
	_contains = items
	empty()
	update_grid()
