extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var cell = load("res://Scenes/Cell.tscn")
var zoom_ratio = 1
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_released("mouse_wheel_up"):
		print_debug("mouse_wheel_up")
		get_node("Player").updateCamera(-0.2)
	if Input.is_action_just_released("mouse_wheel_down"):
		get_node("Player").updateCamera(0.2)
		print_debug("mouse_wheel_down")
