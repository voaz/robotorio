extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var index = 0
export var speed = 0.5
signal item_on_boarder(item)
func on_frame_changed():
	pass
	#print("on_frame_changed called")
	#playing = true
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _ready():
	print_debug("Success: belt created: ", frames.get_frame("New Anim", 0).get_size().x)

func _process(delta):
	if get_child_count() == 0:
		return	
	for child in get_children():
		var pos_x = child.position.x + child.texture.get_size().x
		var pos_y = frames.get_frame("New Anim", 0).get_size().x
		if pos_x < pos_y:
			child.position.x += speed
		else:
			emit_signal("item_on_boarder", child)
#if belt can take more items, change parentes
func on_item_on_boarder(item):
	item.get_parent().remove_child(item)
	item.position.x = -item.texture.get_size().x
	add_child(item)
	
