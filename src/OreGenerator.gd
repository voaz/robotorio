extends Node2D
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var oreFieldSize = 5
export var areaStarts : Vector2
export var areaEnds : Vector2
var pos: Vector2
onready var gen : RandomNumberGenerator = RandomNumberGenerator.new()
# Called when the node enters the scene tree for the first time.
func _ready():
	gen.randomize()
	pos.x = min(gen.randi_range(areaStarts.x, areaEnds.x), areaEnds.x - oreFieldSize)
	pos.y = min(gen.randi_range(areaStarts.y, areaEnds.y), areaEnds.y - oreFieldSize)
	
	pass # Replace with function body.

func placeOre():
	#var ore : Ore = Ore.new()
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
