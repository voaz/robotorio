extends Node2D
class_name Crafter

enum state {
	NO_RECIPE = 0, #reciepe not set
	OUT_OF_SOURCE = 1,
	PROGRESS = 2,
	FINISHED = 3,
	PAUSED = 4,
	OVERFLOW = 5
}
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var progress = 0
var is_source_holded = false
var current_state = state.NO_RECIPE

var isReceiptSelected = false
var stored_resources = {} #itemId : count
var target_item = {} #Item

signal finished(item, count)
signal paused()
signal started()
signal deposit(item_id, count)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# if recipe is set then check if source is sufficient and start producing
func set_target_item(item_id):
	target_item = GlobalData.items[item_id]
	current_state = state.OUT_OF_SOURCE


func update_source(items): # map{id:amount}
	stored_resources = items

# Establish all necessary connections
func initialize_connects(obj):
	connect("finished", obj, "on_craft_finished")
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if current_state == state.NO_RECIPE:
		return
	if current_state == state.OUT_OF_SOURCE:
		var keys = target_item.recipe.keys()
		for iid in keys:
			if stored_resources.has(iid):
				if stored_resources[iid] < target_item.recipe[iid]:
					return
			else:
				return
		for idd in target_item.recipe.keys():
			emit_signal("deposit", idd, target_item.recipe[idd])
		is_source_holded = true

		current_state = state.PROGRESS
		for iid in keys:
			stored_resources[iid] -= target_item.recipe[iid]
		return
	if current_state == state.PROGRESS:
		progress += delta
		if progress > target_item.craft_time:
			progress = 0
			current_state = state.OUT_OF_SOURCE
			is_source_holded = false
			emit_signal("finished", target_item.id, target_item.producing_amount)
		return
