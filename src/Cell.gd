extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var index = 0
export var item = {}

signal clicked(item)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func add_item(item):
	var box = find_node("Box") as VBoxContainer
	self.item = item
	if item == null && box.get_child_count() > 0:
		box.remove_child(box.get_child(0))
	else:
		box.add_child(item)

func get_item():
	var box = find_node("Box") as VBoxContainer
	if box.get_child_count() == 0:
		return null
	return box.get_child(0)

func set_count(count):	
	var label = find_node("CountLabel") as Label
	if count != null:
		label.text = String(count)
	else: 
		label.text = ""
func _on_Control_gui_input(event):
	var ev = event as InputEvent
	if ev.is_action_pressed("mouse_left"):
		emit_signal("clicked", item)
