extends Control

export var total_slots = 10
var contains = {}
var available_items = {}

signal contains(itemid_count)

# Called when the node enters the scene tree for the first time.
func _ready():
	var raw_json = GlobalData.loadRawJSON("res://Data/inventory.json")
	var parsed = JSON.parse(raw_json)
	if parsed.error == null:
		print(parsed.error_string)
		return
	var data = parsed.result["items"]
	if data is Array:
		for item in data:
			add(item["id"], item["count"])

func _show():
	var item_ids = contains.keys()


func add(item_id, count):
	var item = GlobalData.items[item_id]
	if item == null:
		print("Error: item id " + item_id + " not found");
	if contains.has(item_id):
		contains[item_id] += count
	else:
		contains[item_id] = count
	for i in contains.keys():
		if contains[i] == 0:
			contains.erase(i)
	emit_signal("contains", contains)

func remove(items):
	var first_itemid = items.keys()[0]
	if contains.has(first_itemid):
		if contains[first_itemid] - items[first_itemid] > 0:
			contains[first_itemid] -= items[first_itemid]
		else:
			contains.erase(first_itemid)
		emit_signal("contains", contains)

#request items istead of directly removing it
func request(items):
	pass
#if we have a connected manipulator to this inventory we should send a signal
#about wahta we have
func request_contains():
	pass

func add_listener():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# Slots
func on_craft_finished(item_id, amount):
	print("item added to inventory " + item_id)
	add(item_id, amount)

func on_deposit(item_id, amount):
#	remove({item_id: amount})
	pass

func set_connections(obj):
	connect("contains", obj, "update_source")
	emit_signal("contains", contains)


